<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SocialAuthController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider(Request $request)
    {
        $url = explode('/', url()->current());
        $provider = $url[count($url) -1];

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback()
    {
        $url = explode('/', url()->current());
        $provider = $url[count($url) -2];

        $user = Socialite::driver($provider)->user();
        $this->registerOrLogin($user);

        return redirect()->route('home');
    }

    public function registerOrLogin($data)
    {
        $user = User::where(['email' => $data->email])->first();
        if(!$user)
        {
            $user = new User();

            $user->name = $data->name;
            $user->email = $data->email;
            $user->provider_id = $data->id;
            $user->avatar = $data->avatar;
            $user->save();
        }

        Auth::login($user);
    }
}
