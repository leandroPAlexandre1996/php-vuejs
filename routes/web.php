<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\Login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Login Google
Route::get('login/google/', [App\Http\Controllers\SocialAuthController::class, 'redirectToProvider'])->name('login.google');
Route::get('login/google/callback', [App\Http\Controllers\SocialAuthController::class, 'handleProviderCallback'])->name('handleProviderCallback');

//Login Facebook
Route::get('login/facebook', [App\Http\Controllers\SocialAuthController::class, 'redirectToProvider'])->name('login.facebook');
Route::get('login/facebook/callback', [App\Http\Controllers\SocialAuthController::class, 'handleProviderCallback'])->name('handleProviderCallback');

//Login Github
Route::get('login/github', [App\Http\Controllers\SocialAuthController::class, 'redirectToProvider'])->name('login.github');
Route::get('login/github/callback', [App\Http\Controllers\SocialAuthController::class, 'handleProviderCallback'])->name('handleProviderCallback');
